---
layout: default
title: Download
permalink: /download/
---

# Download

App is available to download at following stores:

[<img src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png" alt="Get it on Google Play" height="70">][google]
[<img src="https://f-droid.org/badge/get-it-on.png" alt="Get it on F-Droid" height="70">][fdroid]
[<img src="{{ '/assets/img/get-it-on-gitlab.png' | relative_url }}" alt="Get it on GitLab" height="70">][gitlab]

**Warning**: All three versions (Google Play, F-Droid and the APKs) are not
compatible (not signed by the same key)! You must uninstall one to install
the other, which will delete all your data.

## Source code releases

Ultrasonic source code releases can be downloaded from [GitLab][gitlab].

## Terms of use

This program is distributed in the hope that it will be useful, but
**without any warranty**; without even the implied warranty of
**merchantability** or **fitness for a particular purpose**. See the [GNU
General Public License](https://opensource.org/licenses/gpl-3.0.html) for more
details.

[google]: https://play.google.com/store/apps/details?id=org.moire.ultrasonic
[fdroid]: https://f-droid.org/packages/org.moire.ultrasonic/
[gitlab]: https://gitlab.com/ultrasonic/ultrasonic/-/releases
